<?php


namespace AnzahTools\QuickThread\XF\Entity;


class User extends XFCP_User
{
    /**
     * @return bool
     */
    public function canQuickThreadUse ()
    {
        return $this->hasPermission('forum', 'at_qt_Use');
    }

}